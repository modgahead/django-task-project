Installation Steps:

* Clone this projects using git command: git clone git@bitbucket.org:modgahead/django-task-project.git

* cd to project: cd django-task-project/milodt/

* Create virtual environment using VirtualenvWrapper: mkvirtualenv milodt_env -a . -r requirements.txt --python=/usr/bin/python3
  (If you don't have the VirtualenvWrapper installed, you can use: virtualenv milodt_env -p /usr/bin/python3 && source ./milodt_env/bin/activate && pip install -r requirements.txt)

* Apply migrations: python manage.py migrate

* Load fixtures: python manage.py loaddata fixtures.json

* Run: python manage.py runserver 0.0.0.0:8888

* Now it should be available here: http://0.0.0.0:8888/


List of features done:
* Users CRUD
* Display list of all users (which not marked as deleted)
* Display allowed/blocked status depending on user birth date
* Display BizzFuzz depending on user's "random number" field
* Export list of users into .xls file


List of TO-DO's:
* Unit-tests