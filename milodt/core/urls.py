from django.conf.urls import url, include
from django.contrib import admin

from users import views as users_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', users_views.UserListView.as_view(), name='home'),
    url(r'^(?P<pk>[\d]+)/$', users_views.UserShowView.as_view(), name='user_show'),
    url(r'^create/$', users_views.UserCreateView.as_view(), name='user_create'),
    url(r'^(?P<pk>[\d]+)/update/$', users_views.UserUpdateView.as_view(), name='user_update'),
    url(r'^(?P<pk>[\d]+)/delete/$', users_views.UserDeleteView.as_view(), name='user_delete'),
    url(r'^export-to-xls/$', users_views.ExportToXLSView.as_view(), name='export_to_xls'),
]
