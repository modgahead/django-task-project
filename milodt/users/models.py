import random
from datetime import date

from dateutils import relativedelta

from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin)
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from users.consumers import ws_message


class CustomUserManager(BaseUserManager):

    """Custom manager for CustomUser."""

    def _create_user(self, username, password,
                     is_staff, is_superuser, **extra_fields):
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        is_active = extra_fields.pop("is_active", False)
        user = self.model(username=username, is_staff=is_staff, is_active=is_active,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password=None, **extra_fields):
        is_staff = extra_fields.pop("is_staff", False)
        return self._create_user(username, password, is_staff, False,
                                 **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        return self._create_user(username, password, True, True, is_active=True,
                                 **extra_fields)


class AbstractCustomUser(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(_('username'), max_length=255,
                                unique=True, db_index=True)
    is_staff = models.BooleanField(
        _('staff status'), default=False, help_text=_(
            'Designates whether the user can log into this admin site.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    first_name = models.CharField(_('first name'), max_length=50, blank=True, default='')
    last_name = models.CharField(_('last name'), max_length=50, blank=True, default='')

    is_active = models.BooleanField(_('active'), default=True, help_text=_(
        'Designates whether this user should be treated as '
        'active. Unselect this instead of deleting accounts.'))
    activation_token = models.CharField(_('activation token'), max_length=32, null=True, blank=True)
    activation_token_dt = models.DateTimeField(_('activation token datetime'), null=True, blank=True)

    pass_recovery_token = models.CharField(_('password recovery token'), max_length=32, null=True, blank=True)
    pass_recovery_token_dt = models.DateTimeField(_('password recovery token datetime'), null=True, blank=True)

    objects = CustomUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        abstract = True

    def get_full_name(self):
        return '%s %s' % (self.first_name, self.last_name)

    def get_short_name(self):
        return self.first_name

    def generate_token(self, tok_type=None):
        import uuid
        token = uuid.uuid4().hex
        if tok_type == 'activation':
            self.activation_token = token
            self.activation_token_dt = timezone.now()
        elif tok_type == 'pass_recovery':
            self.pass_recovery_token = token
            self.pass_recovery_token_dt = timezone.now()
        self.save()
        return token


class CustomUser(AbstractCustomUser):
    birthday = models.DateField()
    rand_number = models.PositiveIntegerField(default=random.randint(1, 100), auto_created=True)
    is_deleted = models.BooleanField(default=False, blank=True)

    @property
    def check_eligible(self):
        if self.birthday < date.today() - relativedelta(years=13):
            return 'allowed'
        else:
            return 'blocked'

    @property
    def check_bizzfuzz(self):
        if self.rand_number % 15 == 0:
            return 'BizzFuzz'
        elif self.rand_number % 3 == 0:
            return 'Bizz'
        elif self.rand_number % 5 == 0:
            return 'Fuzz'
        else:
            return self.rand_number

    class Meta(AbstractCustomUser.Meta):
        swappable = 'AUTH_USER_MODEL'
        ordering = ('-date_joined', 'username',)

    def save(self, *args, **kwargs):
        ws_message(message=None, server_message={"text": "User %s has been saved!" % self.username})
        return super(CustomUser, self).save(*args, **kwargs)
