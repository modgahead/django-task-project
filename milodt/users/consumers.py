from django.http import HttpResponse
from channels.handler import AsgiHandler
from channels import Group

def ws_message(message, **kwargs):
    if message:
        Group("chat").send({
            "text": "[user] %s" % message.content['text'],
        })
    elif 'server_message' in kwargs:
        Group("chat").send({
            "text": kwargs.get('server_message')['text'],
        })
    else:
        Group("chat").send({
            "text": 'Error',
        })

# Connected to websocket.connect
def ws_add(message):
    Group("chat").add(message.reply_channel)

# Connected to websocket.disconnect
def ws_disconnect(message):
    Group("chat").discard(message.reply_channel)
