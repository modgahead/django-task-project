from django import template

register = template.Library()

@register.simple_tag
def check_eligible(user):
    return user.check_eligible

@register.simple_tag
def check_bizzfuzz(user):
    return user.check_bizzfuzz
