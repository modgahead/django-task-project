from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.views.generic import ListView
from django.views.generic.base import View
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView

import django_excel

from users.models import CustomUser
from users import forms

from . import models
from .templatetags import common


class UserListView(ListView):
    model = CustomUser
    template_name = 'home.html'
    queryset = CustomUser.objects.filter(is_deleted=False)


class UserShowView(DetailView):
    model = CustomUser
    template_name = 'user_show.html'
    queryset = CustomUser.objects.filter(is_deleted=False)


class UserCreateView(CreateView):
    model = CustomUser
    template_name = 'user_create.html'
    form_class = forms.CustomUserCreationForm
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        messages.success(self.request, 'User was successfully created.')
        return super(UserCreateView, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, 'Error!')
        return super(UserCreateView, self).form_invalid(form)

class UserUpdateView(UpdateView):
    model = CustomUser
    template_name = 'user_update.html'
    form_class = forms.CustomUserUpdateForm
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        messages.success(self.request, 'User was successfully updated.')
        return super(UserUpdateView, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, 'Error!')
        return super(UserUpdateView, self).form_invalid(form)


class UserDeleteView(View):

    def post(self, request, pk):
        try:
            CustomUser.objects.filter(pk=pk).update(is_deleted=True)
            messages.success(request, 'User was successfully deleted.')
        except CustomUser.DoesNotExist:
            messages.error(request, 'Error occur while deleting user.')
        return HttpResponseRedirect(reverse_lazy('home'))


class ExportToXLSView(View):

    def get(self, request):
        output = [['Username', 'Birthday', 'Eligible', 'Random Number', 'BizzFuzz']]
        for user in CustomUser.objects.filter(is_deleted=False):
            output.append([user.username,
                           user.birthday,
                           user.check_eligible,
                           user.rand_number,
                           user.check_bizzfuzz])
        sheet = django_excel.pe.Sheet(output)
        return django_excel.make_response(sheet, file_type='xls', file_name='export.xls')
